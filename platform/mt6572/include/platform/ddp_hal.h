#ifndef _DDP_HAL_
#define _DDP_HAL_
#include "ddp_data_type.h"

struct DISP_REGION
{
    unsigned int x;
    unsigned int y;
    unsigned int width;
    unsigned int height;
};

enum OVL_LAYER_SOURCE {
    OVL_LAYER_SOURCE_MEM    = 0,
    OVL_LAYER_SOURCE_RESERVED = 1,
    OVL_LAYER_SOURCE_SCL     = 2,
    OVL_LAYER_SOURCE_PQ     = 3,
};

typedef struct _OVL_CONFIG_STRUCT
{
    unsigned int ovl_index;
    unsigned int layer;
	unsigned int layer_en;
    enum OVL_LAYER_SOURCE source;
    unsigned int fmt;
    unsigned int addr;  
    unsigned int vaddr;
    unsigned int src_x;
    unsigned int src_y;
    unsigned int src_w;
    unsigned int src_h;
    unsigned int src_pitch;
    unsigned int dst_x;
    unsigned int dst_y;
    unsigned int dst_w;
    unsigned int dst_h;                  // clip region
    unsigned int keyEn;
    unsigned int key; 
    unsigned int aen; 
    unsigned char alpha;  

    unsigned int isTdshp;
    unsigned int isDirty;

    unsigned int buff_idx;
    unsigned int identity;
    unsigned int connected_type;
    unsigned int security;
}OVL_CONFIG_STRUCT;

struct disp_path_config_struct
{
    unsigned int srcModule; // DISP_MODULE_ENUM

	// if srcModule=RDMA0, set following value, else do not have to set following value
    unsigned int addr; 
    unsigned int inFormat; 
    unsigned int pitch;
    struct DISP_REGION srcROI;    // ROI

    OVL_CONFIG_STRUCT ovl_config;
    struct DISP_REGION bgROI;     // background ROI
    unsigned int bgColor;         // background color

    unsigned int dstModule; // DISP_MODULE_ENUM
    unsigned int outFormat; 
    unsigned int dstAddr;  // only take effect when dstModule=DISP_MODULE_WDMA0 or DISP_MODULE_WDMA1
 
    int srcWidth;
    int srcHeight;
    int dstWidth;
    int dstHeight;
    int dstPitch;
};

typedef enum
{
    DDP_SCENARIO_PRIMARY_DISP,
    DDP_SCENARIO_PRIMARY_MEMOUT,
    DDP_SCENARIO_PRIMARY_ALL,
    DDP_SCENARIO_SUB_DISP,
    DDP_SCENARIO_SUB_MEMOUT,
    DDP_SCENARIO_SUB_ALL,
    DDP_SCENARIO_MHL_DISP,
    DDP_SCENARIO_RDMA0_DISP,
    DDP_SCENARIO_RDMA2_DISP,
    DDP_SCENARIO_MAX    
} DDP_SCENARIO_EUNM;

struct disp_path_config_struct_ex
{
    DDP_SCENARIO_EUNM scenario;

    // for ovl
    OVL_CONFIG_STRUCT ovl_config[4];

    // for rdma
    unsigned int src_x;
    unsigned int src_y;
    unsigned int src_w;
    unsigned int src_h;
    unsigned int src_addr;
    unsigned int src_fmt;
    unsigned int src_pitch;

    // for wdma
    unsigned int dst_w;
    unsigned int dst_h;
    unsigned int clip_x;
    unsigned int clip_y;
    unsigned int clip_w;
    unsigned int clip_h;
    unsigned int dst_addr;
    unsigned int dst_fmt;
    unsigned int dst_pitch;
};

struct disp_path_config_mem_out_struct
{
    unsigned int enable;
    unsigned int dirty;
	unsigned int outFormat; 
    unsigned int dstAddr;
    struct DISP_REGION srcROI;        // ROI
};


int disp_wait_timeout(unsigned int flag, unsigned int timeout);
int disp_path_config(struct disp_path_config_struct* pConfig);
int disp_path_config_layer(OVL_CONFIG_STRUCT* pOvlConfig);
int disp_path_get_mutex(void);
int disp_path_release_mutex(void);
int disp_path_wait_reg_update(void);


int disp_path_config_mem_out(struct disp_path_config_mem_out_struct* pConfig);
int disp_path_config_mem_out_without_lcd(struct disp_path_config_mem_out_struct* pConfig);
void disp_path_wait_mem_out_done(void);
int disp_path_clock_on(char* name);
int disp_path_clock_off(char* name);
void disp_path_clear_mem_out_done_flag(void);
int disp_path_query(void); // return different functions according to chip type

int disp_set_lcd_if(unsigned int path_index, unsigned int dst_module);
int disp_scenario_config(struct disp_path_config_struct_ex* pConfig);

#endif
