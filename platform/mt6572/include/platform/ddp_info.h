#ifndef _H_DDP_INFO_
#define _H_DDP_INFO_
//#include <linux/types.h>
#include "mt_typedefs.h"
#include "ddp_data_type.h"
#include "lcm_drv.h"
typedef enum
{
    DISP_MODULE_UFOE   ,
    DISP_MODULE_AAL    ,
    DISP_MODULE_COLOR0 ,
    DISP_MODULE_COLOR1 ,
    DISP_MODULE_RDMA0  ,
    DISP_MODULE_RDMA1  ,  // 5
    DISP_MODULE_RDMA2  ,
    DISP_MODULE_WDMA0  ,
    DISP_MODULE_WDMA1  ,
    DISP_MODULE_OVL0   ,
    DISP_MODULE_OVL1   ,  // 10
    DISP_MODULE_GAMMA  ,
    DISP_MODULE_PWM0   ,
    DISP_MODULE_PWM1   ,
    DISP_MODULE_OD     ,
    DISP_MODULE_MERGE  ,  // 15
    DISP_MODULE_SPLIT0 ,
    DISP_MODULE_SPLIT1 ,
    DISP_MODULE_DSI0,
    DISP_MODULE_DSI1,
    DISP_MODULE_DSIDUAL, // 20
    DISP_MODULE_DPI,
    DISP_MODULE_MEM,      
    DISP_MODULE_CONFIG,
    DISP_MODULE_CMDQ,
    DISP_MODULE_MUTEX,		// 25
    DISP_MODULE_UNKNOWN,
    DISP_MODULE_NUM
} DISP_MODULE_ENUM;

char* ddp_get_module_name(DISP_MODULE_ENUM module);
int   ddp_get_module_max_irq_bit(DISP_MODULE_ENUM module);
/*
#ifndef bool
typedef unsigned char  bool;
#endif
*/
#ifndef NULL
#define NULL  (0)
#endif



typedef enum {
    SOF_SINGLE = 0,
    SOF_DSI0,
    SOF_DSI1,
    SOF_DPI0,
} MUTEX_SOF;  

typedef enum
{
	CMDQ_DISABLE = 0,
	CMDQ_ENABLE
}CMDQ_SWITCH;

typedef enum
{
	CMDQ_BEFORE_STREAM_SOF,
	CMDQ_WAIT_STREAM_EOF_EVENT,
	CMDQ_CHECK_IDLE_AFTER_STREAM_EOF,
	CMDQ_AFTER_STREAM_EOF
}CMDQ_STATE;

enum OVL_LAYER_SOURCE {
    OVL_LAYER_SOURCE_MEM    = 0,
    OVL_LAYER_SOURCE_RESERVED = 1,
    OVL_LAYER_SOURCE_SCL     = 2,
    OVL_LAYER_SOURCE_PQ     = 3,
};

typedef struct _OVL_CONFIG_STRUCT
{
    unsigned int ovl_index;
    DISP_MODULE_ENUM module;
    unsigned int layer;
	unsigned int layer_en;
    enum OVL_LAYER_SOURCE source;
    unsigned int fmt;
    unsigned int addr;  
    unsigned int vaddr;
    unsigned int src_x;
    unsigned int src_y;
    unsigned int src_w;
    unsigned int src_h;
    unsigned int src_pitch;
    unsigned int dst_x;
    unsigned int dst_y;
    unsigned int dst_w;
    unsigned int dst_h;                  // clip region
    unsigned int keyEn;
    unsigned int key; 
    unsigned int aen; 
    unsigned char alpha;  

    unsigned int isTdshp;
    unsigned int isDirty;

    unsigned int buff_idx;
    unsigned int identity;
    unsigned int connected_type;
    unsigned int security;
}OVL_CONFIG_STRUCT;


typedef struct
{
	unsigned int layer;
	unsigned int layer_en;
	unsigned int fmt;
	unsigned int addr;  
	unsigned int vaddr;
	unsigned int src_x;
	unsigned int src_y;
	unsigned int src_w;
	unsigned int src_h;
	unsigned int src_pitch;
	unsigned int dst_x;
	unsigned int dst_y;
	unsigned int dst_w;
	unsigned int dst_h;                  // clip region
	unsigned int keyEn;
	unsigned int key; 
	unsigned int aen; 
	unsigned char alpha;  

	unsigned int isTdshp;
	unsigned int isDirty;

	unsigned int buff_idx;
	unsigned int identity;
	unsigned int connected_type;
	unsigned int security;
}disp_input_config;


typedef struct _RDMA_CONFIG_STRUCT
{
    unsigned idx;            // instance index
    DpColorFormat inputFormat;
    unsigned address;
    unsigned pitch;
    unsigned width; 
    unsigned height; 
}RDMA_CONFIG_STRUCT;


typedef struct _WDMA_CONFIG_STRUCT
{
    DpColorFormat inputFormat; 
    unsigned srcWidth; 
    unsigned srcHeight;     // input
    unsigned clipX; 
    unsigned clipY; 
    unsigned clipWidth; 
    unsigned clipHeight;    // clip
    DpColorFormat outputFormat; 
    unsigned dstAddress; 
    unsigned dstWidth;     // output
    unsigned int useSpecifiedAlpha; 
    unsigned char alpha;
}WDMA_CONFIG_STRUCT;


typedef struct
{
    unsigned int idx;
    // for ovl
    unsigned int ovl_dirty;
    unsigned int rdma_dirty;
    unsigned int wdma_dirty;
    unsigned int dst_dirty;
    OVL_CONFIG_STRUCT  ovl_config[4];
    RDMA_CONFIG_STRUCT rdma_config;
    WDMA_CONFIG_STRUCT wdma_config;	
    LCM_DSI_PARAMS dsi_config;
    unsigned int dst_w;
    unsigned int dst_h;
}disp_ddp_path_config;

typedef struct
{
	DISP_MODULE_ENUM	module;
	int (*init)(DISP_MODULE_ENUM module, void *handle);
	int (*deinit)(DISP_MODULE_ENUM module, void *handle);
	int (*config)(DISP_MODULE_ENUM module, disp_ddp_path_config *config, void *handle);
	int (*start)(DISP_MODULE_ENUM module, void *handle);
	int (*trigger)(DISP_MODULE_ENUM module, void *cmdq_handle);
	int (*stop)(DISP_MODULE_ENUM module, void *handle);
	int (*reset)(DISP_MODULE_ENUM module, void *handle);
	int (*power_on)(DISP_MODULE_ENUM module, void *handle);
	int (*power_off)(DISP_MODULE_ENUM module, void *handle);
	int (*is_idle)(DISP_MODULE_ENUM module);
	int (*is_busy)(DISP_MODULE_ENUM module);
	int (*dump_info)(DISP_MODULE_ENUM module);
	int (*bypass)(DISP_MODULE_ENUM module, int bypass);	
	int (*build_cmdq)(DISP_MODULE_ENUM module, void *cmdq_handle, CMDQ_STATE state);	
	int (*set_lcm_utils)(DISP_MODULE_ENUM module, LCM_DRIVER *lcm_drv);
	int (*polling_irq)(DISP_MODULE_ENUM module, int bit, int timeout);
} DDP_MODULE_DRIVER;



#endif


