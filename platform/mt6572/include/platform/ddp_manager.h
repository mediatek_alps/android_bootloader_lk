#ifndef __DDP_PATH_MANAGER_H__
#define __DDP_PATH_MANAGER_H__

#include "ddp_info.h"
#include "disp_event.h"
#include "ddp_path.h"

#define	DSI_STATUS_REG_ADDR			0xF401B01C
#define	DSI_STATUS_IDLE_BIT			0x80000000

#define RDMA0_STATUS_REG_ADDR       (DISP_REG_RDMA_INT_STATUS)
#define RDMA1_STATUS_REG_ADDR       (DISP_INDEX_OFFSET+ DISP_REG_RDMA_INT_STATUS)
#define RDMA2_STATUS_REG_ADDR       (2*DISP_INDEX_OFFSET+DISP_REG_RDMA_INT_STATUS)

#define MAKE_DDP_IRQ_BIT(module, shift)		((module<<16)|(0x1<<shift))
#define IRQBIT_MODULE(irqbit)                   (irqbit >> 16)
#define IRQBIT_BIT(irqbit)                      (irqbit & 0xffff)

typedef enum
{
    DDP_IRQ_RDMA0_REG_UPDATE =      (DISP_MODULE_RDMA0 <<16  | 0x1<<0),
    DDP_IRQ_RDMA0_START =           (DISP_MODULE_RDMA0 <<16  | 0x1<<1),
    DDP_IRQ_RDMA0_DONE  =           (DISP_MODULE_RDMA0 <<16  | 0x1<<2),
    DDP_IRQ_RDMA0_UNDERFLOW =       (DISP_MODULE_RDMA0 <<16  | 0x1<<3),
    DDP_IRQ_RDMA0_TARGET_LINE =     (DISP_MODULE_RDMA0 <<16  | 0x1<<4),
    
    DDP_IRQ_RDMA1_REG_UPDATE =      (DISP_MODULE_RDMA1 <<16  | 0x1<<0),
    DDP_IRQ_RDMA1_START =           (DISP_MODULE_RDMA1 <<16  | 0x1<<1),
    DDP_IRQ_RDMA1_DONE  =           (DISP_MODULE_RDMA1 <<16  | 0x1<<2),
    DDP_IRQ_RDMA1_UNDERFLOW =       (DISP_MODULE_RDMA1 <<16  | 0x1<<3),
    DDP_IRQ_RDMA1_TARGET_LINE =     (DISP_MODULE_RDMA1 <<16  | 0x1<<4),

    DDP_IRQ_RDMA2_REG_UPDATE =      (DISP_MODULE_RDMA2 <<16  | 0x1<<0),
    DDP_IRQ_RDMA2_START =           (DISP_MODULE_RDMA2 <<16  | 0x1<<1),
    DDP_IRQ_RDMA2_DONE  =           (DISP_MODULE_RDMA2 <<16  | 0x1<<2),
    DDP_IRQ_RDMA2_UNDERFLOW =       (DISP_MODULE_RDMA2 <<16  | 0x1<<3),
    DDP_IRQ_RDMA2_TARGET_LINE =     (DISP_MODULE_RDMA2 <<16  | 0x1<<4),
    
    DDP_IRQ_WDMA0_FRAME_COMPLETE =  (DISP_MODULE_WDMA0<<16   | 0x1<<0),
    DDP_IRQ_WDMA1_FRAME_COMPLETE =  (DISP_MODULE_WDMA1<<16   | 0x1<<0),

    DDP_IRQ_UNKNOW =                (DISP_MODULE_UNKNOWN<<16 | 0x1<<0),

}DDP_IRQ_BIT;

typedef void* disp_path_handle;

int dpmgr_init(void);

disp_path_handle dpmgr_create_path(DDP_SCENARIO_ENUM scenario, void * cmdq_handle);
int dpmgr_destroy_path(disp_path_handle dp_handle);

int dpmgr_path_set_dst_module(disp_path_handle dp_handle,DISP_MODULE_ENUM dst_module);
int dpmgr_path_get_dst_module(disp_path_handle dp_handle);

int dpmgr_path_init(disp_path_handle dp_handle, int encmdq);
int  dpmgr_path_deinit(disp_path_handle dp_handle, int encmdq);
int dpmgr_path_start(disp_path_handle dp_handle, int encmdq);
int dpmgr_path_stop(disp_path_handle dp_handle, int encmdq);
int dpmgr_path_reset(disp_path_handle dp_handle,int encmdq);

int dpmgr_path_config(disp_path_handle dp_handle, disp_ddp_path_config * config, int encmdq);

int dpmgr_path_flush(disp_path_handle dp_handle,int encmdq);


int dpmgr_path_clock_on(disp_path_handle dp_handle,int encmdq);
int dpmgr_path_clock_off(disp_path_handle dp_handle,int encmdq);

int dpmgr_check_status(disp_path_handle dp_handle);


int dpmgr_signal_event(disp_path_handle dp_handle, DISP_PATH_EVENT event);
int dpmgr_enable_event(disp_path_handle dp_handle, DISP_PATH_EVENT event);
int dpmgr_disable_event(disp_path_handle dp_handle, DISP_PATH_EVENT event);
int dpmgr_map_event_to_irq(disp_path_handle dp_handle, DISP_PATH_EVENT event, DDP_IRQ_BIT irq_bit);
int dpmgr_wait_event_timeout(disp_path_handle dp_handle, DISP_PATH_EVENT event, int timeout);
int dpmgr_path_power_on(disp_path_handle handle, CMDQ_SWITCH encmdq);
int dpmgr_path_power_off(disp_path_handle handle, CMDQ_SWITCH encmdq);

int dpmgr_path_set_video_mode(disp_path_handle handle, int is_vdo_mode);
int dpmgr_set_lcm_utils(disp_path_handle handle, void *lcm_drv);
int dpmgr_path_is_busy(disp_path_handle handle);
int dpmgr_path_trigger(disp_path_handle dp_handle, void *trigger_loop_handle);


#endif
