#ifndef _H_DDP_LOG_
#define _H_DDP_LOG_
//#include "xlog.h"
#define DDP_DFINFO 0
#include <debug.h>
#ifndef LOG_TAG
#define LOG_TAG "unknow"
#endif

#if 0
#define DISP_LOG_V( fmt, arg...)   xlog_printk(ANDROID_LOG_VERBOSE, "[DDP/"LOG_TAG"]",  fmt, ##arg)
#define DISP_LOG_D( fmt, arg...)   xlog_printk(ANDROID_LOG_DEBUG,   "[DDP/"LOG_TAG"]",  fmt, ##arg)
#define DISP_LOG_I( fmt, arg...)   xlog_printk(ANDROID_LOG_INFO,    "[DDP/"LOG_TAG"]",  fmt, ##arg)
#define DISP_LOG_W( fmt, arg...)   xlog_printk(ANDROID_LOG_WARN,    "[DDP/"LOG_TAG"]",  fmt, ##arg)
#define DISP_LOG_E( fmt, arg...)   xlog_printk(ANDROID_LOG_ERROR,   "[DDP/"LOG_TAG"]",  fmt, ##arg)
#define DISP_LOG_F( fmt, arg...)   xlog_printk(ANDROID_LOG_FATAL,   "[DDP/"LOG_TAG"]",  fmt, ##arg)
#endif

#define DDPMSG(string, args...)    dprintf(DDP_DFINFO,"[DDP/"LOG_TAG"]"string,##args)  // default on, important msg, not err
#define DDPERR(string, args...)    dprintf(DDP_DFINFO,"[DDP/"LOG_TAG"]error:"string,##args)  //default on, err msg
#define DDPDBG(string, args...)		dprintf(DDP_DFINFO,"[DDP/"LOG_TAG"]"string,##args)  // default on, important msg, not err
#endif